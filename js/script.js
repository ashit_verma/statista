console.log('External script implemented successfully');

function loadFizzBuzzList() {
    let fizzBuzzList = document.getElementById('fizzBuzzList');
    for (let index = 1; index <= 100; index++) {
        let text = index; let className = null;
        if (index % 3 == 0 && index % 5 == 0) {
            text = "fizzbuzz";
            className = "list__fuzz-buzz";
        } else if (index % 3 == 0) {
            text = "fizz";
            className = "list__fizz";
        } else if (index % 5 == 0) {
            text = "buzz";
            className = "list__buzz";
        }
        let div = document.createElement("div");
        div.className = className;
        div.innerHTML = text; fizzBuzzList.appendChild(div);
    };
}
